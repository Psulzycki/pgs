﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGS
{
    public partial class Form1 : Form
    {
        private int Move = 1;
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "")
            {
                MessageBox.Show("Wprowadz wszystkie dane ;)", "Bląd!");
            }
            else
            {
                PersonalData personalData = new PersonalData(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
                Form2 form2 = new Form2(personalData);
                form2.ShowDialog();
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            Move++;
            Motion();
            button3.Enabled = true;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (Move == 0)
            {
                Move = 3;
                Motion();
            }else if (Move == 1)
            {
                Move = 4;
                Motion();
            }
            else
            {
                Move--;
                Motion();
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            using (var ctx = new PersonalDataDbContext())
            {
                ctx.PersonalData.Add(new PersonalData(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text));
                ctx.SaveChanges();
            }
        }

        private void Motion()
        {
            switch (Move)
            {
                case 1:
                    textBox4.Enabled = false;
                    textBox1.Enabled = true;
                    textBox2.Enabled = false;
                    break;
                case 2:
                    textBox1.Enabled = false;
                    textBox2.Enabled = true;
                    textBox3.Enabled = false;
                    break;
                case 3:
                    textBox2.Enabled = false;
                    textBox3.Enabled = true;
                    textBox4.Enabled = false;
                    break;
                case 4:
                    textBox3.Enabled = false;
                    textBox4.Enabled = true;
                    textBox1.Enabled = false;
                    Move = 0;
                    break;

            }
        }


    }
}
